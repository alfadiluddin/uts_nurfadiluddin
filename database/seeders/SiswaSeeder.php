<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Siswa;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        siswa::create ([
            'nama'=> 'aryo adiwijoyo',
            'absen'=> '1',
            'kelas'=> 'ipa',
        ]);
        siswa::create ([
            'nama'=> 'rifki alfarisi',
            'absen'=> '2',
            'kelas'=> 'ips',
        ]);
    }
}
