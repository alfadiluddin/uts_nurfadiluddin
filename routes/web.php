<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiswaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('data', [SiswaController::class, 'index']);
Route::get('data/{id}', [SiswaController::class, 'show']);
Route::get('buat', [SiswaController::class, 'create']);
Route::post('simpan', [SiswaController::class, 'store']);
Route::get('edit/{id}', [SiswaController::class, 'edit']);
Route::post('update/{id}', [SiswaController::class, 'update']);
Route::get('hapus/{id}', [SiswaController::class, 'destroy']);
